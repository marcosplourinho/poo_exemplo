<?php

include 'pessoa.php';

 $pessoa1 = new Pessoa();
 $pessoa1->setNome("Robson");
 $pessoa1->setNascimento(2000);

 echo "Meu nome é ". $pessoa1->getNome() ." <br>"; //técnica de concatenação de Objeto e HTML na String
 echo "Meu ano de nascimento é ". $pessoa1->getNascimento()." <br>";
 echo "Então minha idade é ". $pessoa1->calculaIdade($pessoa1->getNascimento(), 2016)." <br>"; //o objeto Ano de Nascimento é passado como parâmetro no método lugar de uma variável ano.
 echo "Esta pessoa é: ". $pessoa1->verificaMaioridade($pessoa1->calculaIdade($pessoa1->getNascimento(), 2016))." <br>";

// A técnica utiliza vai usar uma function dentro da outra... o "verificaMaioridade(calculaIdade())" vai usar o dado que
// retorna na function interno e usar como parametro de verificação na function externa

 $pessoa2 = new Pessoa();
 $pessoa2->setNome("Marcos");
 $pessoa2->setNascimento(1993);

 echo "<hr>";

 echo "<li> Nome: ". $pessoa2->getNome() ." </li>";
 echo "<li> Ano de Nascimento: ". $pessoa2->getNascimento()." </li>";
 echo "<li> Idade: ". $pessoa2->calculaIdade($pessoa2->getNascimento(), 2016)." </li>";
 echo "<li> Situação: ". $pessoa2->verificaMaioridade($pessoa2->calculaIdade($pessoa2->getNascimento(), 2016))." </li>";
// eu posso instanciar 1 milhão de pessoas no mundo.php sem precisar reescrever variáveis e nem reescrever functions;

?>
