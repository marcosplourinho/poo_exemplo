<?php

class Pessoa {

  private $nome;
  private $nascimento;

  public function setNome($v_nome){
      $this->nome = $v_nome;
  }

  public function getNome(){
    return $this->nome;
  }

  public function setNascimento($v_ano){
      $this->nascimento = $v_ano;
  }

  public function getNascimento(){
    return $this->nascimento;
  }

  public function calculaIdade($nascimento, $ano_atual){
      return $ano_atual - $nascimento;
  }

  public function verificaMaioridade($idade_atual){

    if ($idade_atual >= 18){
      return "Adulto, maior de 18";
    } else {
      return "Menor de idade";
    }
  }

}

?>
